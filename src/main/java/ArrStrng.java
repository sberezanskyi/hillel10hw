//5. Create ArrayList of Strings. Add 5 different Strings to it.
//Using any cycle print all the values of the ArrayList to the console.


import java.util.ArrayList;

public class ArrStrng{
    public static void main(String args[]){
        ArrayList<String> list=new ArrayList<String>();
        list.add("Candy");
        list.add("Apple");
        list.add("Banana");
        list.add("Grapes");
        list.add("Pepsi");


        for (String object: list) {
            System.out.println(object);
        }

    }
}




