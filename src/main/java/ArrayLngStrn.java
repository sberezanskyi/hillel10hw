//6. Create ArrayList of Strings. Add 10 different Strings to it.
//   Using any cycle find the longest String in the list and print it out.


public class ArrayLngStrn {

    public static String getLongestString(String[] array) {
        int maxLength = 0;
        String longestString = null;
        for (String s : array) {
            if (s.length() > maxLength) {
                maxLength = s.length();
                longestString = s;
            }
        }
        return longestString;
    }

    public static void main(String[] args) {
        String[] stroka = {"Banasd16516516516516fsdnn", "Pepsdfsdperoni", "Bldsfsdfack", "98j98j", "qwe dcqwc","cqwcwqcqwc","cwqcwqcwqcq",""};
        String longestString = getLongestString(stroka);
        System.out.format("longest string: '%s'\n", longestString);
    }

}