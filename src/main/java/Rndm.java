//4. Create an array of 15 random integers from the segment [0; 9].
// Display the array on the screen.
// Count how many in the array of even elements and display this number on the screen on a separate line.

import java.util.Arrays;

public class Rndm {

    public static void main(String[] args)
    {
        int even = 0;
        int[] numbers = new int[15];
        for(int i = 0; i < numbers.length; i++) {
            numbers[i] = (int)(Math.random()*9 + 0);
        }
        for(int i = 0; i < numbers.length; i++) {
            if(numbers[i] % 2 == 0)
            {
                even++;
            }

        }

        System.out.printf("Number of even elements in the array: %d\n",even);
        System.out.print(Arrays.toString(numbers));
    }


}
