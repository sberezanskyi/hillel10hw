//10. Create enum for days of the week.

import java.util.Scanner;

public class EnmWeek {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Какой день хочешь ?");
        Integer numberOfDay = scanner.nextInt();


        switch (numberOfDay) {

            case (1):
                System.out.println("Сегодня " + DayEnum.MONDAY);
                break;
            case (2):
                System.out.println("Сегодня " + DayEnum.TUESDAY);
                break;
            case (3):
                System.out.println("Сегодня " + DayEnum.WEDNESDAY);
                break;
            case (4):
                System.out.println("Сегодня " + DayEnum.THURSDAY);
                break;
            case (5):
                System.out.println("Сегодня " + DayEnum.FRIDAY);
                break;
            case (6):
                System.out.println("Сегодня " + DayEnum.SATURDAY);
                break;
            case (7):
                System.out.println("Сегодня " + DayEnum.SUNDAY);
                break;
            default:
                System.out.println("Нема такого");
                break;

        }
    }

    enum DayEnum {
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY

    }
}


