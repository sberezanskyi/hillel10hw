//7. Create HashMap<String, String> and add 10 pairs:
//   Print the content of the map, each pair from a new line.


import java.util.HashMap;
import java.util.Map;

public class HshMap {
    public static void main(String[] args)
    {
        HashMap<String, String> map = new HashMap<>();

        map.put("watermelon", "berry");
        map.put("banana", "fruit");
        map.put("cherry", "berry");
        map.put("pineapple", "fruit");
        map.put("melon", "vegetable");
        map.put("cranberry", "berry");
        map.put("apple", "fruit");
        map.put("iris", "flower");
        map.put("potato", "vegetable");
        map.put("carrot", "vegetable");

        for (Map.Entry<String, String> e : map.entrySet())
            System.out.println(e.getKey() + " - " + e.getValue());
    }
} 