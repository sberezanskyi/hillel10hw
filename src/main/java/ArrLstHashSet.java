//9. Create ArrayList<Integer> and add 100 random numbers (Use Math.random()).
//   Create HashSet<Integer> and add all unique numbers from the ArrayList.

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class ArrLstHashSet {

    public static void main(String[] args) {

        ArrayList<Integer> aListNumbers = new ArrayList<>(100);

        for(int i = 0; i < aListNumbers.size(); i++) {
            aListNumbers[i] = (int)(Math.random()*99 + 0);
        }
        aListNumbers.add(i);


        HashSet<String> hSetNumbers = new HashSet(aListNumbers);

        System.out.println("ArrayList Unique Values");


        for(String strNumber : hSetNumbers)
            System.out.println(strNumber);
    }


}
